#ifndef NAIVE_H
#define NAIVE_H

#include<stdio.h>
#include<stdlib.h>

void findSmoothIntsModC (unsigned long start, unsigned int size, unsigned short *smoothNumbers, unsigned short numberRoots, short *roots, unsigned short maxRoot, unsigned short degree, short *poly, unsigned int C, unsigned short *numberSmoothIntsModC, unsigned long *smoothIntsModC, unsigned short maxNumberResults);

#endif
