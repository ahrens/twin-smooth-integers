# Sage implementaion

The different files contain Sage code for parts of the computation and don't run independently. They are separated to add more structure but have to be combined to run properly.

PTE Solutions defines different solutions that were used for the benchmarks.

Sieve of Eratosthenes defines the functions that are used to mark the elements in the search interval as smooth (1) or not (0).

Naive and Opptinised Approach contain all the functions needed for the corresponding algorithm.

Example shows a possible way to combine the other functions to a working algorithm to search for twin smooth integers.
(Sieve of Eratosthenes, Naive Approach, Opptinised Approach should be included in front of Example to provide the needed functions.)
