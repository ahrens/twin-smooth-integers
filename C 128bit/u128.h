#ifndef U128_H
#define U128_H

#include<stdio.h>
#include<stdlib.h>

void printu128 (unsigned __int128 u128);

unsigned __int128 pow2(unsigned short exponent);

#endif
